package fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;

import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;


public class MinhasProvasAdapter extends RecyclerView.Adapter<MinhasProvasViewHolder>  {
    private int position;
    private Context context;
    private List<Prova> provas = new ArrayList<>();
    private OnItemClickListener listener;


    private List<String> listaRa;


    public MinhasProvasAdapter(Context context, List<Prova> provas){
        this.context = context;
        this.provas = provas;
    }

    @NonNull
    @Override
    public MinhasProvasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.design_lista_minhas_provas,parent,false);

        MinhasProvasViewHolder holder = new MinhasProvasViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MinhasProvasViewHolder holder, int position) {

        if(provas.size() > 0){
            Prova prova = provas.get(position);
            listaRa = Persistence.getDb(context).respostaDAO().getListaRA(prova.getId());

            Persistence.getDb(context).respostaDAO().getListaRA(prova.getId());
            holder.nomeDaProva.setText(prova.getNomeDaProva());
            holder.modeloGabarito.setText(context.getString(R.string.modelo)+" "+prova.getModeloDeGabarito());
            holder.dataProva.setText(context.getString(R.string.data)+" "+prova.getDataDaProva());
            holder.totalCorrecoes.setText(""+listaRa.size());

            holder.listener = listener;
            this.position = position;

        }

    }

    @Override
    public int getItemCount() {
        return provas.size();

    }


    public void setOnClickListener(OnItemClickListener listener){
        this.listener = listener;
    }




}
