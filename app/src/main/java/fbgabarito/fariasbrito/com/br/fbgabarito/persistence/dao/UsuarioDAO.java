package fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;

@Dao
public interface UsuarioDAO {

    @Insert
    public void inserirUsuario(Usuario usuario);

    @Update
    public void atualizarUsuario(Usuario usuario);

    @Delete
    public void removerUsuario(Usuario usuario);

    @Query("SELECT id FROM usuarios WHERE id == :id")
    public Usuario getUsuarioID(int id);

    @Query("SELECT * FROM usuarios")
    public List<Usuario> getUsuarios();


    @Query("SELECT * FROM usuarios WHERE email_usuario == :email" )
    public Usuario getUsuario(String email);


}
