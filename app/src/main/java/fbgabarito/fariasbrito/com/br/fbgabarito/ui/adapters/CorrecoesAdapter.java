package fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;
import fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies.CorrecoesActivity;

public class CorrecoesAdapter extends RecyclerView.Adapter <CorrecoesViewHolder>{

    public Context context;
    public List<String> listaRa = new ArrayList<>();
    public List<Resposta> listaResposta = new ArrayList<>();

    public CorrecoesAdapter(Context context, List<String> listaRa){
        this.context = context;
        this.listaRa = listaRa;
    }


    @NonNull
    @Override
    public CorrecoesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layout = LayoutInflater.from(context);
        View v = layout.inflate(R.layout.design_correcoes, parent,false);
        CorrecoesViewHolder holder = new CorrecoesViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CorrecoesViewHolder holder, int position) {
        if(listaRa.size() > 0){
            int acertos = 0;
            int erros = 0;
            String ra = listaRa.get(position);

            listaResposta = Persistence.getDb(context).respostaDAO().getRespostasRa(ra);
            int id = 0;
            Questao questao = null;
            for(Resposta resposta:listaResposta){
                 questao = Persistence.getDb(context).questaoDAO().getQuestaoId(resposta.getQuestaoId());
                if(resposta.getOpcaoMarcada().equalsIgnoreCase(questao.getOpcaoCorreta())){
                    acertos ++;
                }


            }

            holder._ra.setText(ra);
            holder._qtd.setText(listaResposta.size()+"");
            holder._certas.setText(acertos+"");
            holder._erradas.setText((listaResposta.size() - acertos)+"");

        }
    }

    @Override
    public int getItemCount() {
        return listaRa.size();
    }
}
