package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.arch.persistence.room.Room;

import android.content.Intent;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;

import android.support.design.widget.Snackbar;

import android.support.v7.app.AlertDialog;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.FBGabaritoDB;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao.UsuarioDAO;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;
import fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters.MinhasProvasAdapter;

import fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters.OnItemClickListener;



public class MinhasProvasActivity extends AppCompatActivity implements OnItemClickListener,DialogInterface.OnClickListener {
    private RecyclerView _lstProvas;
    private TextView _sem_proova;
    private AlertDialog dialogSair;
    private Usuario usuario;
    private AlertDialog alertaOpcoes;
    private int position;
    private AlertDialog confirmacaoExcluir;
    MinhasProvasAdapter adapter;

    private List<Prova> provas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minhas_provas);

        _sem_proova = (TextView)findViewById(R.id.txt_sem_prova);
        _lstProvas = (RecyclerView)findViewById(R.id.lstProvas);
        _sem_proova.setVisibility(View.GONE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        _lstProvas.setLayoutManager(linearLayoutManager);
        _lstProvas.setHasFixedSize(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.minhas_provas_menu,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.item_inserir_prova){
            Intent it =new Intent(getApplicationContext(),NovaProvaActivity.class);
            it.putExtra("idProva",0);
            startActivity(it);

        } else if(item.getItemId() == R.id.item_sair){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.aviso));
            builder.setMessage(getString(R.string.logout));
            builder.setPositiveButton(getString(R.string.sim), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    SharedPreferences.Editor editor = Persistence.getPreferences(getApplicationContext()).edit();
                    editor.putBoolean(getString(R.string.logado), false);
                    editor.putInt(getString(R.string.id_usuario), 0);
                    editor.apply();
                    finish();

                }
            });
            builder.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogSair.dismiss();
                }
            });
            dialogSair = builder.create();
            dialogSair.show();

        }else if(item.getItemId() == R.id.apagarConta){


                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Aviso");
                builder.setMessage("Deseja realmente excluir sua conta? ");
                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        excluirUsuario();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        SharedPreferences.Editor editor = Persistence.getPreferences(getApplicationContext()).edit();
                        editor.putBoolean(getString(R.string.logado), false);
                        editor.putInt(getString(R.string.id_usuario), 0);
                        editor.apply();
                        Toast.makeText(getApplicationContext(), "Usuário removido com sucesso", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });
                builder.setNegativeButton(getString(R.string.nao), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogSair.dismiss();
                    }
                });
                dialogSair = builder.create();
                dialogSair.show();
            }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = Persistence.getPreferences(this);

        provas =  Persistence.getDb(this).provaDAO().getProvasDeUsuario(preferences.getInt(getString(R.string.id_usuario),0));

        if(provas.size() == 0){
            _sem_proova.setVisibility(View.VISIBLE);
        }else{
            _sem_proova.setVisibility(View.GONE
            );

        }
         adapter = new MinhasProvasAdapter(getApplicationContext(),provas);
        adapter.setOnClickListener(this);
        _lstProvas.setAdapter(adapter);
    }


    @Override
    public void click(int position) {
        this.position = position;
        alertaOpcoes = alert();
        alertaOpcoes.show();
    }

    public AlertDialog alert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Opções");
        CharSequence[] opcoes = {"Detalhes","Atualizar","Excluir"};
        builder.setItems( opcoes,MinhasProvasActivity.this);
        return builder.create();

    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {

            case 0:
                Intent it = new Intent(this,DetalhesProvaActivity.class);
                SharedPreferences.Editor editor = Persistence.getPreferences(this).edit();
                editor.putInt(getString(R.string.id_prova),provas.get(position).getId());
                editor.apply();
                startActivity(it);
            break;
            case 1:
                Intent i =new Intent(getApplicationContext(),NovaProvaActivity.class);
                i.putExtra("idProva",provas.get(position).getId());
                startActivity(i);
            break;
            case 2:
                alertaOpcoes.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Atenção");
                builder.setMessage("Deseja realmete excluir?");
                builder.setPositiveButton("Sim",MinhasProvasActivity.this);
                builder.setNegativeButton("Não",MinhasProvasActivity.this);
                confirmacaoExcluir = builder.create();
                confirmacaoExcluir.show();
            break;
            case DialogInterface.BUTTON_POSITIVE:
                excluirProva();
              break;
            case DialogInterface.BUTTON_NEGATIVE:
                confirmacaoExcluir.dismiss();
             break;
        }

    }

    public void excluirProva(){

        Prova prova = provas.get(position);
        List<Questao> questoes = Persistence.getDb(this).questaoDAO().getQuestaoDaProva(prova.getId());
        List<Resposta> respostas = Persistence.getDb(this).respostaDAO().getListaResposta(prova.getId());
        Persistence.getDb(this).respostaDAO().removerRespostas(respostas);
        Persistence.getDb(this).questaoDAO().removerQuestoes(questoes);
        Persistence.getDb(this).provaDAO().removerProva(prova);
        Toast.makeText(this,"Excluida com sucesso",Toast.LENGTH_SHORT).show();
        this.provas.remove(position);
        adapter.notifyDataSetChanged();
    }

    public void excluirUsuario(){
        int id_usario = Persistence.getPreferences(this).getInt(getString(R.string.id_usuario),0);
        usuario = Persistence.getDb(this).usuarioDAO().getUsuarioID(id_usario);

        for(Prova prova:provas){
            List<Questao>questoes = Persistence.getDb(this).questaoDAO().getQuestaoDaProva(prova.getId());
            List<Resposta> respostas = Persistence.getDb(this).respostaDAO().getListaResposta(prova.getId());
            Persistence.getDb(this).respostaDAO().removerRespostas(respostas);
            Persistence.getDb(this).questaoDAO().removerQuestoes(questoes);
        }
        Persistence.getDb(this).provaDAO().removerProvas(provas);

        Persistence.getDb(this).usuarioDAO().removerUsuario(usuario);
    }
}
