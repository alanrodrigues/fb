package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;

public class DigitarCorrigirAcitvity extends AppCompatActivity {
    String ra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getRA(ra);
    }

    public void getRA(String codigo){
        ra = codigo;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirme o RA");
        View view = getLayoutInflater().inflate(R.layout.desing_ra,null);
        Button _btnOk =  view.findViewById(R.id.button_ok);
        final EditText _edt_ra = view.findViewById(R.id.editText_ra);
        _edt_ra.setText(ra);
        builder.setView(view);
        final AlertDialog alertDialog =  builder.create();
        alertDialog.show();
        _btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ra = _edt_ra.getText().toString();
                if(!ra.equals("")){
                    Intent dados = new Intent(getApplicationContext(), CorrigirProvaActivity.class);
                    dados.putExtra("ra", ra);
                    startActivity(dados);
                    alertDialog.dismiss();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Insira o RA",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
