package fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

import android.arch.persistence.room.ForeignKey;

import android.arch.persistence.room.PrimaryKey;

import java.util.Date;


@Entity(tableName = "provas",foreignKeys = @ForeignKey(entity = Usuario.class,parentColumns = "id",childColumns = "usuario_id"))

public class Prova {
    @PrimaryKey
    private int id;

    @ColumnInfo(name = "data_prova")
    private String dataDaProva;

    @ColumnInfo(name = "gabarito_prova")
    private String modeloDeGabarito;

    @ColumnInfo(name = "nome_Prova")
    private String nomeDaProva;


    @ColumnInfo(name = "usuario_id")
    private int usuarioId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeDaProva() {
        return nomeDaProva;
    }

    public void setNomeDaProva(String nomeDaProva) {
        this.nomeDaProva = nomeDaProva;
    }

    public String getDataDaProva() {
        return dataDaProva;
    }

    public void setDataDaProva(String dataDaProva) {
        this.dataDaProva = dataDaProva;
    }

    public String getModeloDeGabarito() {
        return modeloDeGabarito;
    }

    public void setModeloDeGabarito(String modeloDeGabarito) {
        this.modeloDeGabarito = modeloDeGabarito;
    }


    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

}
