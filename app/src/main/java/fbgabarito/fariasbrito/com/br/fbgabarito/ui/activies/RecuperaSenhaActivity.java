package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;

public class RecuperaSenhaActivity extends AppCompatActivity {

    private EditText _txtEmail;
    private Button _btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recupera_senha);

        _txtEmail = (EditText) findViewById(R.id.edtEmail);
        _btnEntrar = (Button) findViewById(R.id.btnEntrar);


       _btnEntrar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               validaCampos(v);
           }
       });
    }

    private void validaCampos(View view){
        boolean res = false;

        String email = _txtEmail.getText().toString();

        if(res = !isEmailValido(email)){
            _txtEmail.requestFocus();
        }
        if(res){

            Snackbar.make( view, "Preencha o campo email corretamente!", Snackbar.LENGTH_SHORT).show();

        }else{

            Usuario user = Persistence.getDb(this).usuarioDAO().getUsuario(email.toLowerCase());
            if(user != null){

                if(email.toLowerCase().equals(user.getEmail())){
                    Intent it = new Intent(this,NovaSenhaActivity.class);
                    it.putExtra("email",email);
                    startActivity(it);

//                    SharedPreferences.Editor editor = MainActivity.preferences.edit();
//                    editor.putBoolean(getString(R.string.logado),true);
//                    editor.putInt(getString(R.string.id_usuario),users.get(0).getId());
//                    editor.apply();
//                    finish();

                }else{
                    Snackbar.make( view, "Email incorreto", Snackbar.LENGTH_SHORT).show();
                }
            }else{
                Snackbar.make( view, "Usuário não está cadastrado", Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isCampoVazio(String valor){
        boolean res = (TextUtils.isEmpty(valor) || valor.trim().isEmpty());
        return res;
    }

    private boolean isEmailValido(String email){
        boolean res = (!isCampoVazio(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        return res;
    }

}
