package fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;

public class CorrecoesViewHolder extends RecyclerView.ViewHolder{


    public TextView _ra, _qtd, _certas, _erradas;

    public CorrecoesViewHolder(View view) {
        super(view);
        _ra = (TextView)view.findViewById(R.id.textview_desing_ra);
        _qtd = (TextView)view.findViewById(R.id.textview_desing_qtd);
        _certas = (TextView)view.findViewById(R.id.textview_desing_qtdCertas);
        _erradas = (TextView)view.findViewById(R.id.textview_desing_qtdErradas);

    }
}
