package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import fbgabarito.fariasbrito.com.br.fbgabarito.R;

public class DialogData extends DialogFragment{
    public void DialogData(){

    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int  ano = calendar.get(Calendar.YEAR);
        int mes = calendar.get(Calendar.MONTH);
        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),listener,ano,mes,dia);
    }
    DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar date = Calendar.getInstance();
            date.set(year, month, dayOfMonth);
            String data = getString(R.string.data_da_prova) + " " + new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(date.getTime());
        }
    };
}
