package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;


import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;

import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class DetalhesProvaActivity extends AppCompatActivity {

    private CardView _cadastrarGabarito,_correcoes,_card_corrigir_prova,_card_relatorio_turma,_card_enviar_mapa,_card_exportar_mapa;
    private int provaId;
    private List<Questao> questoesDaProva = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes_prova);
        _card_corrigir_prova = (CardView) findViewById(R.id.card_corrigir_prova);
        _card_relatorio_turma = (CardView) findViewById(R.id.card_relatorio_turma);
        _cadastrarGabarito = (CardView) findViewById(R.id.cadastrarGabarito);
        _correcoes = (CardView) findViewById(R.id.correcoes);
        _card_enviar_mapa = (CardView) findViewById(R.id.card_enviar_mapa);
        _card_exportar_mapa = (CardView)findViewById(R.id.card_exportar_mapa);

        _cadastrarGabarito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    Intent it = new Intent(DetalhesProvaActivity.this, CadastroGabaritoActivity.class);
                    startActivity(it);

            }
        });
        _correcoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gabaritoCadastrado() >0){
                    Intent it = new Intent(DetalhesProvaActivity.this, CorrecoesActivity.class);
                    startActivity(it);
                }else{

                    Toast.makeText(getApplicationContext(),"Por favor, cadastre o gabarito.",Toast.LENGTH_SHORT).show();
                }
            }
        });
        _card_corrigir_prova.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(gabaritoCadastrado() >0){
                    Intent it = new Intent(DetalhesProvaActivity.this, CameraActivity.class);
                    startActivity(it);
                }else{
                    Toast.makeText(getApplicationContext(),"Por favor, cadastre o gabarito.",Toast.LENGTH_SHORT).show();
                }

            }
        });
        _card_relatorio_turma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(gabaritoCadastrado() >0){
                    Intent it = new Intent(DetalhesProvaActivity.this, RelatorioTurmaActivity.class);
                    startActivity(it);
                }else{
                    Toast.makeText(getApplicationContext(),"Por favor, cadastre o gabarito.",Toast.LENGTH_SHORT).show();
                }

            }
        });
        _card_enviar_mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(gabaritoCadastrado() >0){
                    if(provasCorrigidas() > 0){
                        Intent it = new Intent(DetalhesProvaActivity.this, EnviarMapaRespostaActivity.class);
                        startActivity(it);
                    }else{
                        Toast.makeText(getApplicationContext(),"Nenhuma prova corrigida.",Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(getApplicationContext(),"Por favor, cadastre o gabarito.",Toast.LENGTH_SHORT).show();
                }
            }
        });
        _card_exportar_mapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(gabaritoCadastrado() >0){
                    if(provasCorrigidas() > 0){
                        String resposta = Persistence.exportarPlanilha(getApplicationContext());
                        if(resposta.equalsIgnoreCase("erro")){
                            mensagem("Erro ao exportar");
                        }else{
                            mensagem(resposta);
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Nenhuma prova corrigida.",Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(getApplicationContext(),"Por favor, cadastre o gabarito.",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void mensagem(String texto){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aviso");
        builder.setMessage( texto);
        builder.setNegativeButton("Ok",null);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public int gabaritoCadastrado(){
        provaId = Persistence.getPreferences(getApplicationContext()).getInt(getString(R.string.id_prova),0);
        questoesDaProva = Persistence.getDb(getApplicationContext()).questaoDAO().getQuestaoDaProva(provaId);
        return questoesDaProva.size();
    }
    public int provasCorrigidas(){
        List<String> listaRa = Persistence.getDb(getApplicationContext()).respostaDAO().getListaRA(provaId);
        return listaRa.size();
    }




}


