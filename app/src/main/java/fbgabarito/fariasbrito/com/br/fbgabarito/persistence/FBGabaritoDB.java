package fbgabarito.fariasbrito.com.br.fbgabarito.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao.ProvaDAO;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao.QuestaoDAO;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao.RespostaDAO;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao.UsuarioDAO;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;

@Database(entities = {Prova.class, Usuario.class, Questao.class,Resposta.class},version = 1)
public abstract class FBGabaritoDB extends RoomDatabase{
    public abstract ProvaDAO provaDAO();
    public abstract UsuarioDAO usuarioDAO();
    public abstract QuestaoDAO questaoDAO();
    public abstract RespostaDAO respostaDAO();
}
