package fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters;

public interface OnItemClickListener {
     void click(int position);
}
