package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.os.Environment;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;


public class EnviarMapaRespostaActivity extends AppCompatActivity {
    private EditText _edit_para,_edit_assunto;
    String emissor = "jaoltj00@gmail.com";
    String senha = "ltj87579511";
    int idProva;
    private Prova prova;
    Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_mapa_resposta);
        _edit_para = (EditText)findViewById(R.id.edit_enviar_email_para);
        _edit_assunto = (EditText)findViewById(R.id.edit_enviar_email_assunto);
        idProva = Persistence.getPreferences(this).getInt(getString(R.string.id_prova),0);
        prova = Persistence.getDb(this).provaDAO().getProva(idProva);


    }
    public void limparCampos(){
        _edit_para.setText("");
        _edit_assunto.setText("");
    }
    public void enviarMapaResposta(View view){
        if(isEmailValido(_edit_para.getText().toString().trim())){
           final Button btn = (Button)view;
            btn.setText("Enviando...");
            btn.setEnabled(false);

                    try{

                        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                        StrictMode.setThreadPolicy(policy);


                        Properties properties = new Properties();
                        properties.put("mail.smtp.host","smtp.googlemail.com");
                        properties.put("mail.smtp.socketFactory.port","465");
                        properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
                        properties.put("mail.smtp.auth","true");
                        properties.put("mail.smtp.port","465");
                        session = Session.getDefaultInstance(properties, new Authenticator() {
                            @Override
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(emissor,senha);
                            }
                        });

                        Persistence.exportarPlanilha(this);

                        String data = prova.getDataDaProva().replace("/","_");
                        String nomeArquivo =    prova.getNomeDaProva()+"_"+data+"_"+prova.getId()+ ".xls";
                        String path = Environment.getExternalStorageDirectory() + "/" +getString(R.string.app_name)+"/"+nomeArquivo;

                        if(session != null){
                            String assunto = _edit_assunto.getText().toString();
                            Message message = new MimeMessage(session);
                            message.setFrom(new InternetAddress(emissor));
                            message.setSubject(getString(R.string.app_name));
                            message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(_edit_para.getText().toString()));

                          //assunto da mensagem
                            Multipart multipart = new MimeMultipart();
                            BodyPart messageBodyPart = new MimeBodyPart();
                            messageBodyPart.setContent(assunto, "text/plain");
                            multipart.addBodyPart(messageBodyPart);

                            File file = new File(path);
                            DataSource ds = new FileDataSource(file) {

                                public String getContentType() {
                                    return "application/octet-stream";
                                }
                            };

                            BodyPart mbp = new MimeBodyPart();
                            mbp.setDataHandler(new DataHandler(ds));
                            mbp.setFileName(file.getName());
                            mbp.setDisposition(Part.ATTACHMENT);
                            multipart.addBodyPart(mbp);

                            message.setContent(multipart);
                            Transport.send(message);

                        }


                                Toast.makeText(getApplicationContext(),"Enviado com sucesso",Toast.LENGTH_SHORT).show();
                                finish();



                    }catch (SendFailedException e){

                       Snackbar.make(view,"Email não existe",Snackbar.LENGTH_SHORT).show();
                        Log.i("JOAOSANTOS", e.toString());

                    }catch (Exception e){

                        Snackbar.make(view,"Desculpe, ocorreu um erro tente novamente",Snackbar.LENGTH_SHORT).show();
                        Log.i("JOAOSANTOS", e.toString());
                    }finally {
                        btn.setText("Enviar");
                        btn.setEnabled(true);
                        limparCampos();
                    }

        }else{
            Snackbar.make(view,"Email inválido",Snackbar.LENGTH_SHORT).show();
        }

    }
    private boolean isCampoVazio(String valor){
        boolean res = (TextUtils.isEmpty(valor) || valor.trim().isEmpty());
        return res;
    }
    private boolean isEmailValido(String email){
        boolean res = (!isCampoVazio(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        return res;
    }
}
