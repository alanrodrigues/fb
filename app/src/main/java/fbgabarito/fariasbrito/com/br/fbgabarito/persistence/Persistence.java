package fbgabarito.fariasbrito.com.br.fbgabarito.persistence;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Persistence {

    private static FBGabaritoDB db;
    private static SharedPreferences preferences;
    private static List<String> listaRa  = new ArrayList<>();

    public static FBGabaritoDB getDb(Context context){
        if(db == null){
            db = Room.databaseBuilder(context,FBGabaritoDB.class,context.getString(R.string.dbName)).allowMainThreadQueries().build();
        }
        return db;
    }

    public static SharedPreferences getPreferences(Context context){
        if(preferences == null) {
            preferences = context.getSharedPreferences(context.getString(R.string.preferences), context.MODE_PRIVATE);
        }
        return preferences;
    }

    public static String exportarPlanilha(Context context){
        try{

            int idProva = Persistence.getPreferences(context).getInt(context.getString(R.string.id_prova),0);
            Prova prova = Persistence.getDb(context).provaDAO().getProva(idProva);
            listaRa = Persistence.getDb(context).respostaDAO().getListaRA(idProva);
            String path = criarPasta(context);

            String data = prova.getDataDaProva().replace("/","_");
            String nomeArquivo =    prova.getNomeDaProva()+"_"+data+"_"+prova.getId()+ ".xls";
            File file = new File(path,nomeArquivo);
            WritableWorkbook wb = Workbook.createWorkbook(file);
            WritableSheet planilha = wb.createSheet(prova.getNomeDaProva(),0);

            Label lblNumeroRa = new Label(0,0,"Nº:RA");
            planilha.addCell(lblNumeroRa);
            int row = 1;
            boolean loopInical = true;
            for(String ra: listaRa){
                List<Resposta> respostas = new ArrayList<>();
                respostas = Persistence.getDb(context).respostaDAO().getRespostasRa(ra);
                int col =1;
                Label lblRa = new Label(0,row,ra);
                for(Resposta resposta:respostas){
                    if(loopInical){
                        Questao q = Persistence.getDb(context).questaoDAO().getQuestaoId(resposta.getQuestaoId());
                        Label lblNumero = new Label(col,0,q.getNumero()+"");
                        planilha.addCell(lblNumero);
                    }
                    Label lblOpcao = new Label(col,row,resposta.getOpcaoMarcada());
                    planilha.addCell(lblOpcao);
                    col++;
                }
                loopInical = false;
                row++;
                planilha.addCell(lblRa);
            }

            wb.write();
            wb.close();
            return "Salvo em: "+path;

        }catch(Exception e){
            Log.i("JOAOSANTOS",e.toString());
            return "erro";
        }

    }

    public static String criarPasta(Context context){
        String path = Environment.getExternalStorageDirectory() + "/" +context.getString(R.string.app_name);
        File pasta = new File(path);
        if (!pasta.exists()) {
            pasta.mkdir();
        }
        return path;
    }


}
