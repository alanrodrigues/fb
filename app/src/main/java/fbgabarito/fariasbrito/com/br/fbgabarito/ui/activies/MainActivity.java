package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.Manifest;
import android.arch.persistence.room.Room;

import android.content.Intent;
import android.content.SharedPreferences;

import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.FBGabaritoDB;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_CAMERA_PERMISSION = 12;
    private final int REQUEST_INTERNET_PERMISSION = 1;
    private final int REQUEST_STORAGE_PERMISSION = 16;
    private final int REQUEST_STORAGE_PERMISSION_READ = 15;
    private int permitidos =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        permissoesOk();


    }
    public void permissoesOk(){
        if(Persistence.getPreferences(this).getBoolean("permissoes",false)){
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        }
    }
    public void inserirPreference(){
        if(permitidos == 2){
            SharedPreferences.Editor edit = Persistence.getPreferences(this).edit();
            edit.putBoolean("permissoes",true);
            edit.apply();
            permissoesOk();

        }
        Log.i("JOAOSANTOS",""+permitidos);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permitidos++;
                   inserirPreference();

                }
                return;

            case  REQUEST_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permitidos++;
                    inserirPreference();

                }
                return;
            case REQUEST_STORAGE_PERMISSION_READ:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permitidos++;
                    inserirPreference();

                }
                return;


        }
    }

    public void acessoCamera(View view){
        permissao(Manifest.permission.CAMERA,REQUEST_CAMERA_PERMISSION);
    }
    public void acessoInternet(View view){
        permissao(Manifest.permission.INTERNET,REQUEST_INTERNET_PERMISSION);
    }
    public void acessoArmazenamento(View view){
        permissao(Manifest.permission.WRITE_EXTERNAL_STORAGE,REQUEST_STORAGE_PERMISSION);
    }
    public void permissao(String permissao,int REQUEST_PERMISSION){
        if(ContextCompat.checkSelfPermission(this,permissao) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permissao}, REQUEST_PERMISSION);
        }
    }
}
