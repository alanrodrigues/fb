package fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "questao",foreignKeys = @ForeignKey(entity = Prova.class,parentColumns = "id",childColumns = "prova_id"))
public class Questao {

    @PrimaryKey
    private int id;

    @ColumnInfo(name = "numero_questao")
    private int numero;

    @ColumnInfo(name = "opcao_correta_questao")
    private String opcaoCorreta;

    @ColumnInfo(name = "prova_id")
    private int provaId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getOpcaoCorreta() {
        return opcaoCorreta;
    }

    public void setOpcaoCorreta(String opcaoCorreta) {
        this.opcaoCorreta = opcaoCorreta;
    }

    public int getProvaId() {
        return provaId;
    }

    public void setProvaId(int provaId) {
        this.provaId = provaId;
    }
}
