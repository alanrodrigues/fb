package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.Intent;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;


import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.FBGabaritoDB;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;




public class LoginActivity extends AppCompatActivity {

    private Button btnEntrar;
    private TextView txtRecuperaSenha;
    private TextView txtPrimeiroAcesso;
    private EditText edtEmail;
    private EditText edtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setContentView(R.layout.activity_login);

        edtEmail = (EditText)findViewById(R.id.txtEmail);
        edtSenha = (EditText)findViewById(R.id.txtSenha);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        txtPrimeiroAcesso = (TextView) findViewById(R.id.txtPrimeiroAce);
        txtRecuperaSenha = (TextView) findViewById(R.id.esqueciSenha);


        btnEntrar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                validaCampos(v);

            }
        });

        txtPrimeiroAcesso.setOnClickListener(new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            Intent primeiroAcesso = new Intent(LoginActivity.this, PrimeiroAcessoActivity.class);
            startActivity(primeiroAcesso);
        }
    });

        txtRecuperaSenha.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent recuperarSenha = new Intent(LoginActivity.this, RecuperaSenhaActivity.class);
                startActivity(recuperarSenha);
            }
        });
    logado();


    }

    private void validaCampos(View view){
        boolean res = false;

        String email = edtEmail.getText().toString();
        String senha = edtSenha.getText().toString();


            if(res = !isEmailValido(email)){

                edtEmail.requestFocus();
            }else if(res = isCampoVazio(senha)) {
                edtSenha.requestFocus();

            }

        if(res){

            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setTitle("Aviso");
            dlg.setMessage("Há campos inválidos e/ou em branco!");
            dlg.setNeutralButton("Ok", null);
            dlg.show();
        }else{
                    Usuario user = Persistence.getDb(this).usuarioDAO().getUsuario(email.toLowerCase());
                    if (user != null) {

                        if (email.toLowerCase().equals(user.getEmail()) && senha.equals(user.getSenha())) {
                            startActivity(new Intent(this, MinhasProvasActivity.class));

                            SharedPreferences.Editor editor = Persistence.getPreferences(this).edit();
                            editor.putBoolean(getString(R.string.logado), true);
                            editor.putInt(getString(R.string.id_usuario), user.getId());
                            editor.apply();
                            finish();

                        }

                        else {
                            Snackbar.make(view, "Senha e/ou email incorretos", Snackbar.LENGTH_SHORT).show();
                        }
                    } else {

                        Snackbar.make(view, "Usuário não está cadastrado", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }





    private boolean isCampoVazio(String valor){
        boolean res = (TextUtils.isEmpty(valor) || valor.trim().isEmpty());
        return res;
    }

    private boolean isEmailValido(String email){

        boolean res = (!isCampoVazio(email.trim()) && Patterns.EMAIL_ADDRESS.matcher(email).matches());

        return res;
    }


    public void logado(){


        if(Persistence.getPreferences(this).getBoolean(getString(R.string.logado),false)){
           startActivity(new Intent(this,MinhasProvasActivity.class));
            finish();
        }

    }

}

