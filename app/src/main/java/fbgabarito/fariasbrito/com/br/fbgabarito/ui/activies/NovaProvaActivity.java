package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;

public class NovaProvaActivity extends AppCompatActivity {

    private TextView _txtDataProva,_txtModeloGabarito;
    private EditText _nomeDaProva;
    private int ano,mes,dia;
    private AlertDialog alertGabarito;
    private View viewAlert;
    private Prova prova;

    int idProva;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_nova_prova);
        _nomeDaProva = (EditText)findViewById(R.id.edtNomeDaProva);
        _txtDataProva = (TextView) findViewById(R.id.txt_data_prova);
        _txtModeloGabarito = (TextView) findViewById(R.id.txt_modelo_prova);
        Calendar calendar = Calendar.getInstance();
        ano = calendar.get(Calendar.YEAR);
        mes = calendar.get(Calendar.MONTH);
        dia = calendar.get(Calendar.DAY_OF_MONTH);

         idProva = getIntent().getExtras().getInt("idProva",0);
        Log.i("JOAOSANTOS",idProva+"");
        if(idProva != 0){
            iniciarValores();
        }else{
            prova = new Prova();
            prova.setId((int)(-1*System.currentTimeMillis()));
        }


    }

    private void iniciarValores() {
        prova = Persistence.getDb(this).provaDAO().getProva(idProva);
        _nomeDaProva.setText(prova.getNomeDaProva());
        _txtDataProva.setText(prova.getDataDaProva());
        _txtModeloGabarito.setText(prova.getModeloDeGabarito());
    }

    public void exibirDialogData(View view){
        showDialog(view.getId());
       // DialogFragment d = new DialogData();
       // d.show(getFragmentManager(),"tag");
    }
    public void exibirDialogModelo(View view){
        criarAlert();
    }
    public void modeloGabaritoClick(View v) {
        if(v.getId() == R.id.item_10_questoes){
            alterarNomeTextViewGabarito(getString(R.string.questoes_10));
        }
        else if(v.getId() == R.id.item_20_questoes){
            alterarNomeTextViewGabarito(getString(R.string.questoes_20));
        }
        else if(v.getId() == R.id.item_50_questoes){
            alterarNomeTextViewGabarito(getString(R.string.questoes_50));
        }
        else if(v.getId() == R.id.item_100_questoes){
            alterarNomeTextViewGabarito(getString(R.string.questoes_100));
        }
    }

    public void exibirImagemGabaritoClick(View v){
       if(v.getId() == R.id.img_10_questoes){
            monstrarImagemGabarito();
        }
        else if(v.getId() == R.id.img_20_questoes){
            monstrarImagemGabarito();
        }
        else if(v.getId() == R.id.img_50_questoes){
            monstrarImagemGabarito();
        }
        else if(v.getId() == R.id.img_100_questoes){
            monstrarImagemGabarito();
        }else if(v.getId() == R.id.img_gabarito_voltar){
            alertGabarito.dismiss();
            criarAlert();
        }
    }

    public void alterarNomeTextViewGabarito(String questoes){
      _txtModeloGabarito.setText(questoes);
        alertGabarito.dismiss();
    }

    @Override
    public Dialog onCreateDialog(int it){
        return new DatePickerDialog(this,listener,ano,mes,dia);
    }

    DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar date = Calendar.getInstance();
            date.set(year, month, dayOfMonth);

            String data = new SimpleDateFormat("dd/MM/yyyy", Locale.US).format(date.getTime());
            _txtDataProva.setText(data);
        }
    };
    public void monstrarImagemGabarito(){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view= layoutInflater.inflate(R.layout.desing_alert_modelo_gabarito_img,null);
        alertGabarito.setContentView(view);
    }
    public void criarAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Escolha o Modelo");
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        viewAlert = layoutInflater.inflate(R.layout.desing_alert_modelo_gabarito,null);
        builder.setView(viewAlert);
        alertGabarito = builder.create();
        alertGabarito.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.nova_prova_menu,menu);
        return true;
    }

    public boolean validarCampos(String nomeProva,String dataProva,String modeloGabarito){
        if(!modeloGabarito.trim().equals("") && !dataProva.trim().equals("") && !nomeProva.trim().equals("")){
            return true;
        }else{
            String mensagem = null;
            if(nomeProva.equals("")){
                mensagem = "Insira o nome da prova";
            }else if(modeloGabarito.equals("")){
                mensagem = "Selecione o modelo de gabarito";
            }else{
                mensagem = "Selecione a data da prova";
            }
            Toast.makeText(this,mensagem,Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.item_salvar_prova){
            try {
                String dataProva = _txtDataProva.getText().toString();
                String nomeDaProva = _nomeDaProva.getText().toString();
                String modeloGabarito = _txtModeloGabarito.getText().toString();
                int id_usario = Persistence.getPreferences(this).getInt(getString(R.string.id_usuario),0);
                if(validarCampos(nomeDaProva,dataProva,modeloGabarito)){
                    prova.setModeloDeGabarito(modeloGabarito);
                    prova.setNomeDaProva(nomeDaProva);
                    prova.setDataDaProva(dataProva);
                    prova.setUsuarioId(id_usario);
                    if(idProva != 0){
                        Persistence.getDb(this).provaDAO().atualizarProva(prova);
                    }else{
                        Persistence.getDb(this).provaDAO().inserirProva(prova);
                }
                    Toast.makeText(this,"Salva com sucesso",Toast.LENGTH_SHORT).show();
                    finish();
                }
            } catch (Exception e) {

            }
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }
}
