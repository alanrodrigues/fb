package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.Result;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class CodigoBarraActivity extends AppCompatActivity {
    String ra;
    private ZXingScannerView vista;
    private Button _btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_leitor);
        escanear(vista);
    }

    public void escanear(View view){
        vista = new ZXingScannerView(this);
        vista.setResultHandler(new scan());
        setContentView(vista);
        vista.startCamera();
    }

    class scan implements ZXingScannerView.ResultHandler{

        @Override
        public void handleResult(Result result) {
            String dados = result.getText();

            vista.stopCamera();
            getRA(dados);
        }
    }

    public void getRA(String codigo){
        ra = codigo;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirme o RA");
        View view = getLayoutInflater().inflate(R.layout.desing_ra,null);
        Button _btnOk =  view.findViewById(R.id.button_ok);
        final EditText _edt_ra = view.findViewById(R.id.editText_ra);
        _edt_ra.setText(ra);
        builder.setView(view);
        final AlertDialog alertDialog =  builder.create();
        alertDialog.show();
        _btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ra = _edt_ra.getText().toString();
                if(!ra.equals("")){
                    Intent dados = new Intent(getApplicationContext(), CorrigirProvaActivity.class);
                    dados.putExtra("ra", ra);
                    startActivity(dados);
                    alertDialog.dismiss();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Insira o RA",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
