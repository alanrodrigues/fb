package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters.CorrecoesAdapter;
import fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters.MinhasProvasAdapter;

public class CorrecoesActivity extends AppCompatActivity {

    private TextView _idProva;
    private RecyclerView _lstCorrecoes;
    private List<String> listaRa = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_correcoes);

        _lstCorrecoes = (RecyclerView)findViewById(R.id.lstCorrecoes);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        _lstCorrecoes.setLayoutManager(linearLayoutManager);
        _lstCorrecoes.setHasFixedSize(true);

    }

    @Override
    protected void onResume() {
        super.onResume();
        int idprova = Persistence.getPreferences(this).getInt(getString(R.string.id_prova),0);
        listaRa =  Persistence.getDb(this).respostaDAO().getListaRA(idprova);
        CorrecoesAdapter adapter = new CorrecoesAdapter(getApplicationContext(),listaRa);
        _lstCorrecoes.setAdapter(adapter);
    }


}
