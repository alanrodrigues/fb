package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import android.support.design.widget.Snackbar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.List;




import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.FBGabaritoDB;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;
import fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies.MainActivity;

public class PrimeiroAcessoActivity extends AppCompatActivity {

    private EditText txtNome;
    private EditText txtEmail;
    private EditText txtSenha,txtConfirmarSenha;
    private Button btnEntrar;

    private List<Usuario> lista_usuarios = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primeiro_acesso);

        txtNome = (EditText) findViewById(R.id.txtNome);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        txtConfirmarSenha = (EditText) findViewById(R.id.txtConfirmarSenha);
        btnEntrar = (Button) findViewById(R.id.btnSalvar);
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validaCampos(v);

            }
        });
    }




    private void validaCampos(View view){

        boolean res = false;

        String nome = txtNome.getText().toString();
        String email = txtEmail.getText().toString();
        String senha = txtSenha.getText().toString();
        String c_senha =txtConfirmarSenha.getText().toString();

        if(res = isCampoVazio(nome)){
            txtNome.requestFocus();
        }else if(res = !isEmailValido(email)){
            txtEmail.requestFocus();
        }
        else if(res = isCampoVazio(senha)){
            txtSenha.requestFocus();
        }else if(res = isCampoVazio(c_senha)){
            txtConfirmarSenha.requestFocus();
        }
        if(res){
            AlertDialog.Builder dlg = new AlertDialog.Builder(this);
            dlg.setTitle("Aviso");
            dlg.setMessage("Há campos inválidos ou em branco!");
            dlg.setNeutralButton("Ok", null);
            dlg.show();
        }else{
            Usuario usuario = Persistence.getDb(this).usuarioDAO().getUsuario(email.toLowerCase());
            if (usuario != null) {
                Snackbar.make(view, "Email já está cadastrado", Snackbar.LENGTH_SHORT).show();
            } else {
                if (senha.length() > 8){
                if (senha.equals(c_senha)) {
                    Usuario user = new Usuario();
                    user.setId((int) (System.currentTimeMillis() * -1));
                    user.setNome(nome);
                    user.setEmail(email.toLowerCase());
                    user.setSenha(senha);
                    Persistence.getDb(this).usuarioDAO().inserirUsuario(user);
                    Toast.makeText(this, "Usuário cadastrado com sucesso", Toast.LENGTH_SHORT).show();
                    finish();
                }else {
                    Snackbar.make(view, "Senhas não correspondem", Snackbar.LENGTH_SHORT).show();

                }
                } else {

                    Snackbar.make(view, "A senha deve ser maior que 8 caracteres", Snackbar.LENGTH_SHORT).show();
                }

            }


        }

    }

    private boolean isCampoVazio(String valor){
        boolean res = (TextUtils.isEmpty(valor) || valor.trim().isEmpty());
        return res;
    }

    private boolean isEmailValido(String email){
        boolean res = (!isCampoVazio(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
        return res;
    }
}
