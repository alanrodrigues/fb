package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;


import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;

import com.github.mikephil.charting.charts.HorizontalBarChart;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;

public class RelatorioTurmaActivity extends AppCompatActivity {

    private HorizontalBarChart barChart;

    private int idProva;
    private Prova prova;
    private List<String> listaRa = new ArrayList<>();
    private List<Resposta> listaRespostas = new ArrayList<>();
    private Map<Integer,Integer> valoresRelatorio ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_turma);

        idProva = Persistence.getPreferences(this).getInt(getString(R.string.id_prova),0);
        prova = Persistence.getDb(this).provaDAO().getProva(idProva);
        listaRa = Persistence.getDb(this).respostaDAO().getListaRA(idProva);
        valoresRelatorio = new HashMap<>();
        iniciarValores();


        barChart = findViewById(R.id.barChart);

        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.setMaxVisibleValueCount(100);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(true);

        List<BarEntry> barEntries =getEntries();
        BarDataSet barDataSet = new BarDataSet(barEntries,"Acertos");
        barChart.getDescription().setEnabled(false);


        barDataSet.setColor(Color.GREEN);

        BarData data = new BarData(barDataSet);
        data.setBarWidth(0.9f);
        barChart.setData(data);

        XAxis xAxis = barChart.getXAxis();


        xAxis.setValueFormatter(new MyAxixValueFormmatter());

        YAxis yLeft= barChart.getAxisLeft();

        barChart.getRendererLeftYAxis().renderAxisLabels(new Canvas());
        barChart.getAxisRight().setEnabled(false);
        xAxis.setGranularity(1);
        xAxis.setAxisMinimum(1);
        yLeft.setValueFormatter(new MyAxixValueFormmatter());


    }
    public void iniciarValores(){
        for(int i = 1; i <=getQuantidadeQuestoes();i++){
            valoresRelatorio.put(new Integer(i),new Integer(0));
        }
    }

    public int getQuantidadeQuestoes(){
        String modeloDeGabarito = prova.getModeloDeGabarito();
        if(modeloDeGabarito.equalsIgnoreCase(getString(R.string.questoes_10))){
            return 10;
        }else if(modeloDeGabarito.equalsIgnoreCase(getString(R.string.questoes_20))){
            return 20;
        }else if(modeloDeGabarito.equalsIgnoreCase(getString(R.string.questoes_50))){
            return 50;
        }else{
            return 100;
        }
    }

    public List<BarEntry> getEntries(){

        for(String ra : listaRa){
            listaRespostas = Persistence.getDb(this).respostaDAO().getRespostasRa(ra);
            for(Resposta resposta : listaRespostas){
                Questao questao = Persistence.getDb(this).questaoDAO().getQuestaoId(resposta.getQuestaoId());
                if(resposta.getOpcaoMarcada().equalsIgnoreCase(questao.getOpcaoCorreta())){
                    int acertos = valoresRelatorio.get(questao.getNumero());
                    acertos++;
                    valoresRelatorio.put(questao.getNumero(),acertos);
                }
            }
        }
        List<BarEntry> barEntries = new ArrayList<>();
        for(int i =1 ;i <=valoresRelatorio.size();i++){
            int y  = (int)valoresRelatorio.get(new Integer(i));
            BarEntry barEntry = new BarEntry(i,y);
            barEntries.add(barEntry);
        }



        return barEntries;
    }

    public class MyAxixValueFormmatter implements IAxisValueFormatter {
        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return String.valueOf((int)value);
        }
    }
}


