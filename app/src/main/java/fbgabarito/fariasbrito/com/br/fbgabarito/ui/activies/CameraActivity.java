package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.zxing.Result;


import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class CameraActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageButton _buttonTirarFoto;
    private TextureView _textureView;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private String cameraId;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest.Builder cameraRequestBuilder;
    private Size imageDimension;
    private ImageReader imageReader;

    private File file;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private boolean mFlashSupported;
    private Handler mBackggroundHandler;
    private HandlerThread mBackgorundThread;
    CameraDevice.StateCallback stateCallBack = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        _buttonTirarFoto = (ImageButton) findViewById(R.id.button_capturar);
        _textureView = (TextureView) findViewById(R.id.texture_view);
        assert _textureView != null;

        _textureView.setSurfaceTextureListener(textureListener);
        _buttonTirarFoto.setOnClickListener(this);
    }

    private void openCamera() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            manager.openCamera(cameraId, stateCallBack, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        tirarFoto();
    }

    private void tirarFoto() {
        if(cameraDevice == null){
            return;
        }

        CameraManager manager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);

        try{
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraDevice.getId());
            Size[] jpegsizes = null;

            if(characteristics != null){
                jpegsizes = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(ImageFormat.JPEG);
            }

            int width = 640;
            int height = 480;
            if(jpegsizes != null && jpegsizes.length > 0){
                width = jpegsizes[0].getWidth();
                height = jpegsizes[0].getHeight();
            }
            final ImageReader reader = ImageReader.newInstance(width,height,ImageFormat.JPEG,1);
            List<Surface> outputSurface = new ArrayList<>(2);
            outputSurface.add(reader.getSurface());
            outputSurface.add(new Surface(_textureView.getSurfaceTexture()));

            final CaptureRequest.Builder cb = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            cb.addTarget(reader.getSurface());
            cb.set(CaptureRequest.CONTROL_MODE,CameraMetadata.CONTROL_MODE_AUTO);

            int rotation = getWindowManager().getDefaultDisplay().getRotation();

            cb.set(CaptureRequest.JPEG_ORIENTATION,ORIENTATIONS.get(rotation));

            file =  new File(Environment.getExternalStorageDirectory() + "/Android/data/teste2.jpg");
            ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
                @Override
                public void onImageAvailable(ImageReader readers) {
                    Image image = null;
                    try{
                        image = reader.acquireLatestImage();
                        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                        byte[] bytes = new byte[buffer.capacity()];
                        buffer.get(bytes);
                        salvar(bytes);
                    }catch (Exception e){
                        Log.i("JOAOSANTOS",e.getMessage());
                    }finally {
                        if(image != null){
                            image.close();
                        }
                    }

                }

                private void salvar(byte[] bytes) throws Exception{
                    OutputStream outputStream = null;
                    try{
                        outputStream = new FileOutputStream(file);
                        outputStream.write(bytes);
                    }finally {
                        if(outputStream != null){
                            outputStream.close();
                        }
                    }
                }
            };
            reader.setOnImageAvailableListener(readerListener,mBackggroundHandler);
            final CameraCaptureSession.CaptureCallback captureListener = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request,  TotalCaptureResult result) {
                    super.onCaptureCompleted(session, request, result);
                    Toast.makeText(getApplicationContext(),"Salvo",Toast.LENGTH_SHORT).show();
                    createCameraPreview();

                    startActivity(new Intent(getApplicationContext(), OpcaoCorrigirProvaActivity.class));

                }
            };
            cameraDevice.createCaptureSession(outputSurface, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {

                        try {
                            session.capture(cb.build(),captureListener,mBackggroundHandler);
                        } catch (CameraAccessException e) {
                            Log.i("JOAOSANTOS",e.getMessage());
                        }

                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {

                }
            },mBackggroundHandler);

        }catch (CameraAccessException ce){
            Log.i("JOAOSANTOS",ce.getMessage());
        }
    }

    private void createCameraPreview() {

        try{
            SurfaceTexture texture = _textureView.getSurfaceTexture();
            assert  texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(),imageDimension.getHeight());
            Surface surface = new Surface(texture);
            cameraRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            cameraRequestBuilder.addTarget(surface);
            cameraDevice.createCaptureSession(Arrays.asList(surface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    if(cameraDevice == null){
                        return;
                    }
                    cameraCaptureSessions = session;
                    atualizarPreview();
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {
                    Toast.makeText(getApplicationContext(), "Erro", Toast.LENGTH_SHORT).show();
                }
            },null);
        } catch (CameraAccessException e) {
            Log.i("JOAOSANTOS",e.getMessage());
        }

    }

    private void atualizarPreview() {
        if(cameraDevice == null){
            Toast.makeText(getApplicationContext(), "Erro2", Toast.LENGTH_SHORT).show();
        }
        cameraRequestBuilder.set(CaptureRequest.CONTROL_MODE,CaptureRequest.CONTROL_MODE_AUTO);
        try{
           cameraCaptureSessions.setRepeatingRequest(cameraRequestBuilder.build(),null,mBackggroundHandler);
        } catch (CameraAccessException e) {
            Log.i("JOAOSANTOS",e.getMessage());
        }
    }
    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {

        }
    };


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {



                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
  @Override
    public void onResume(){
        super.onResume();

      super.onResume();
      if (!OpenCVLoader.initDebug()) {
          Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
          OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
      } else {
          Log.d("OpenCV", "OpenCV library found inside package. Using it!");
          mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
      }
        startBackgroundTheread();
        if(_textureView.isAvailable()){
            openCamera();
        }else{
            _textureView.setSurfaceTextureListener(textureListener);
        }
  }

    private void startBackgroundTheread() {
      mBackgorundThread = new HandlerThread("Camera Background");
      mBackgorundThread.start();
      mBackggroundHandler = new Handler(mBackgorundThread.getLooper());
    }

    @Override
    protected void onPause(){
      stopBackgroundTheread();
        super.onPause();
    }

    private void stopBackgroundTheread() {
    mBackgorundThread.quitSafely();
    try{
        mBackgorundThread.join();
        mBackgorundThread =null;
        mBackggroundHandler = null;
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
    }

}
