package fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;


import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;

@Dao
public interface QuestaoDAO {

    @Insert
     void inserirQuestao(Questao...questao);

    @Insert
    void inserirQuestaoLista(List<Questao> questao);

    @Update
     void atualizarQuestao(List<Questao> questao);

    @Update
    void atualizarQuestao(Questao questao);

    @Delete
     void removerQuestao(Questao questao);
    @Delete
    void removerQuestoes(List<Questao> questoes);
    @Query("SELECT * FROM Questao")
     List<Questao> getQuestao();

    @Query("SELECT * FROM Questao WHERE id = :id")
    Questao getQuestaoId(int id);


    @Query("SELECT * FROM Questao WHERE prova_id = :id ORDER BY numero_questao ASC ")
    List<Questao> getQuestaoDaProva(int id);

}
