package fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity(tableName = "respostas",foreignKeys = @ForeignKey(entity = Questao.class,parentColumns = "id",childColumns = "questao_id"))
public class Resposta {

    @PrimaryKey
    private int id;

    @ColumnInfo(name = "ra")
    private String ra;

    @ColumnInfo(name = "opcao_marcada")
    private String opcaoMarcada;

    @ColumnInfo(name = "questao_id")
    private int questaoId;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getRa() {
        return ra;
    }

    public void setRa(String ra) {
        this.ra = ra;
    }

    public String getOpcaoMarcada() {
        return opcaoMarcada;
    }

    public void setOpcaoMarcada(String opcaoMarcada) {
        this.opcaoMarcada = opcaoMarcada;
    }

    public int getQuestaoId() {
        return questaoId;
    }

    public void setQuestaoId(int questaoId) {
        this.questaoId = questaoId;
    }
}
