package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;

import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;

public class CadastroGabaritoActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout tblGabarito;
    private  int qtd_questoes,provaId;
    private HashMap<Integer,Button> marcados = new HashMap<Integer,Button>();
    private List<Questao> lista_questoes = new ArrayList<>();
    private boolean jaCadastrado = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_gabarito);

        tblGabarito = (LinearLayout)findViewById(R.id.tblGabarito);
        provaId = Persistence.getPreferences(this).getInt(getString(R.string.id_prova),0);
        Prova prova = Persistence.getDb(this).provaDAO().getProva(provaId);
        String modelo = prova.getModeloDeGabarito();
        lista_questoes = Persistence.getDb(this).questaoDAO().getQuestaoDaProva(provaId);

        if(modelo.equalsIgnoreCase(getString(R.string.questoes_10))){
            qtd_questoes = 10;
        }else if(modelo.equalsIgnoreCase(getString(R.string.questoes_20))){
            qtd_questoes = 20;
        }else if(modelo.equalsIgnoreCase(getString(R.string.questoes_50))){
            qtd_questoes = 50;
        }else if(modelo.equalsIgnoreCase(getString(R.string.questoes_100))){
            qtd_questoes = 100;
        }


        if(lista_questoes.size() > 0)
            jaCadastrado = true;

        gerarLayout();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.cadastro_gabarito_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.item_salvar_gabarito:
                if(lista_questoes.size() == qtd_questoes){

                    if(jaCadastrado){
                        Persistence.getDb(this).questaoDAO().atualizarQuestao(lista_questoes);
                    }else{
                        Persistence.getDb(this).questaoDAO().inserirQuestaoLista(lista_questoes);
                    }
                    Toast.makeText(this,"Salvo com sucesso",Toast.LENGTH_SHORT).show();
                    finish();
                }else{

                    int questao = faltaMarcar();
                    LinearLayout linearLayout = (LinearLayout)findViewById(questao);
                    linearLayout.clearFocus();
                    linearLayout.requestFocus();
                    Toast.makeText(this,"Questão não marcada: "+questao,Toast.LENGTH_SHORT).show();

                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public int faltaMarcar(){
        for(int i =1;i<=qtd_questoes;i++){
            if(!marcados.containsKey(i)){
               return i;
            }
        }
        return 1;
    }

    public void gerarLayout(){

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;


        for(int i = 1;i<=qtd_questoes;i++){
            String questao = (i < 10 ? "0"+i : i+"");


            LinearLayout _colunas = new LinearLayout(this);
            _colunas.setOrientation(LinearLayout.HORIZONTAL);

            _colunas.setLayoutParams(params);
            _colunas.setTag(questao);


            _colunas.setFocusable(true);
            _colunas.setFocusableInTouchMode(true);
            _colunas.setId(i);
            _colunas.setWeightSum(5);

            LinearLayout.LayoutParams paramsTxt = new LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView _numeroQquestao = new TextView(this);
            paramsTxt.weight = 1;
            _numeroQquestao.setLayoutParams(paramsTxt);
            _numeroQquestao.setText(questao);
            _numeroQquestao.setTextSize(20);

            Button _btnA = getButton("A",i);
            Button _btnB = getButton("B",i);
            Button _btnC = getButton("C",i);
            Button _btnD = getButton("D",i);



            _colunas.addView(_numeroQquestao);
            _colunas.addView(_btnA);
            _colunas.addView(_btnB);
            _colunas.addView(_btnC);
            _colunas.addView(_btnD);

            tblGabarito.addView(_colunas);
        }

    }

    public Button getButton(String opcao,int position){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight =1;
        params.rightMargin = 5;
        params.leftMargin = 5;
        params.topMargin = 5;
        params.bottomMargin = 5;
        Button btn = new Button(this);
        btn.setOnClickListener(this);
        btn.setText(opcao);
        btn.setTextSize(15);
        btn.setLeft(100);



        btn.setLayoutParams(params);
        btn.setBackground(getDrawable(R.drawable.botao));

        if(jaCadastrado){
            if(lista_questoes.get((position-1)).getOpcaoCorreta().equals(opcao)){
                btn.setBackground(getDrawable(R.drawable.botao_marcado));
                marcados.put(position,btn);
            }
        }


        return btn;
    }

    @Override
    public void onClick(View view) {
        Button btn = (Button)view;
        LinearLayout linearLayout = (LinearLayout)view.getParent();
        int questao = Integer.valueOf((linearLayout.getTag().toString()));
        String questao_op = btn.getText().toString();

        if(marcados.containsKey(questao)){

            marcados.get(questao).setBackgroundResource(R.drawable.botao);
            int position = 0;

            for(Questao gabarito:lista_questoes){
                if(gabarito.getNumero() == questao){
                    gabarito.setOpcaoCorreta(questao_op);
                    lista_questoes.set(position,gabarito);
                }

                position++;
            }

        }else{
            Questao gabarito = new Questao();
            gabarito.setProvaId(provaId);

            gabarito.setOpcaoCorreta(questao_op);
            gabarito.setId((int)(-1*System.currentTimeMillis()));
            gabarito.setNumero(questao);
            lista_questoes.add(gabarito);
        }

        marcados.put(Integer.valueOf(questao),btn);
        btn.setBackgroundResource(R.drawable.botao_marcado);

    }


}
