package fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;

@Dao
public interface ProvaDAO {

    @Insert
    public void inserirProva(Prova prova);

    @Update
    public void atualizarProva(Prova prova);

    @Delete
    public void removerProva(Prova prova);

    @Delete
    public void removerProvas(List<Prova> prova);


    @Query("SELECT * FROM provas")
    public List<Prova> getProvas();

    @Query("SELECT * FROM provas WHERE usuario_id == :id")
    public List<Prova> getProvasDeUsuario(int id);

    @Query("SELECT * FROM provas WHERE id == :id")
    public Prova getProva (int id);

}
