package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;

public class OpcaoCorrigirProvaActivity extends AppCompatActivity {

    ImageView _qrCode;
    ImageView _text;
    ImageView _barCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opcao_corrigir_prova);

        _qrCode = findViewById(R.id.img_qrcode);
        _barCode = findViewById(R.id.img_barcode);
        _text = findViewById(R.id.img_text);

        _qrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OpcaoCorrigirProvaActivity.this, QrCodeActivity.class));
            }
        });

        _barCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OpcaoCorrigirProvaActivity.this, CodigoBarraActivity.class));
            }
        });

        _text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(OpcaoCorrigirProvaActivity.this, DigitarCorrigirAcitvity.class));
            }
        });
    }
}
