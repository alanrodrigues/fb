package fbgabarito.fariasbrito.com.br.fbgabarito.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;

import android.arch.persistence.room.OnConflictStrategy;

import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;

@Dao
public interface RespostaDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)

     void inserirResposta(Resposta... resposta);
    @Insert
    void inserirResposta(List<Resposta> respostas);

    @Update
     void atualizarResposta(List<Resposta> respostas);
    @Update
    void atualizarResposta(Resposta resposta);
    @Delete
     void removerResposta(Resposta resposta);
    @Delete
    void removerRespostas(List<Resposta> respostas);
    @Query("SELECT * FROM respostas")
     List<Resposta> getResposta();


    @Query("SELECT respostas.id,respostas.ra,respostas.opcao_marcada,respostas.questao_id FROM respostas INNER JOIN questao ON respostas.questao_id = questao.id WHERE respostas.ra = :ra ORDER BY questao.numero_questao ASC ")
    List<Resposta> getRespostasRa(String ra);

    @Query("SELECT respostas.id,respostas.ra,respostas.opcao_marcada,respostas.questao_id  FROM respostas INNER JOIN questao ON respostas.questao_id = questao.id and questao.prova_id = :idprova")
    List<Resposta> getListaResposta(int idprova);

    @Query("SELECT ra FROM respostas INNER JOIN questao ON respostas.questao_id = questao.id and questao.prova_id = :idprova  GROUP BY respostas.ra")
    List<String> getListaRA(int idprova);



}
