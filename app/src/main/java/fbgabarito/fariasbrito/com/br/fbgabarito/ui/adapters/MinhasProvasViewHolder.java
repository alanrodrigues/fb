package fbgabarito.fariasbrito.com.br.fbgabarito.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import fbgabarito.fariasbrito.com.br.fbgabarito.R;
public class MinhasProvasViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    public TextView nomeDaProva,modeloGabarito,dataProva, totalCorrecoes;

    public OnItemClickListener listener;
    int position;
    public MinhasProvasViewHolder(View view) {
        super(view);

        totalCorrecoes =  view.findViewById(R.id.design_txt_total_correcoes);

        nomeDaProva = (TextView)view.findViewById(R.id.desing_lista_minhas_provas_txt_nome);
        modeloGabarito = (TextView)view.findViewById(R.id.desing_lista_minhas_provas_txt_modelo);
        dataProva = (TextView)view.findViewById(R.id.desing_lista_minhas_provas_txt_data);
       view.setOnClickListener(this);
       this.position = getAdapterPosition();
    }


    @Override
    public void onClick(View v) {
        if(listener != null){
            listener.click(getAdapterPosition());
        }
    }
}