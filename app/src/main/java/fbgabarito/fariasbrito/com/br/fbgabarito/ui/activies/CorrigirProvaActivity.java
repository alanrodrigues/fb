package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.sun.mail.imap.protocol.UID;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;


import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;
import org.opencv.utils.Converters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;

import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Prova;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Questao;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Resposta;


public class CorrigirProvaActivity extends AppCompatActivity implements View.OnClickListener {

    private final String[] OPCOES = {"A","B","C","D"};
    private boolean isSalva = false;



    String ra;

    private TextView _textview_gabaritoAluno,_textview_gabaritoCerto,_textview_acertos;
    private List<Resposta> respostasAluno  = new ArrayList<>();
    private List<Questao> questoesCertas = new ArrayList<>();
    private List<String> listaRa = new ArrayList<>();
    private List<Resposta> respostaSalvas = new ArrayList<>();
    private ProgressBar _progressbar_progresso;
    private Button _button_corrigir,_button_salvar_resposta;




    int qtdQuestoes ;
    int provaId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leitor);

        Bundle dados = getIntent().getExtras();
        ra = dados.getString("ra");

        _button_salvar_resposta = (Button)findViewById(R.id.button_salvar_resposta);
        _button_corrigir = (Button)findViewById(R.id.button_corrigir);
        _button_corrigir.setOnClickListener(this);
        _textview_gabaritoAluno = (TextView)findViewById(R.id.textview_gabaritoAluno);
        _textview_gabaritoCerto = (TextView)findViewById(R.id.textview_gabaritoCerto);
        _textview_acertos = (TextView)findViewById(R.id.textview_acertos);
        _progressbar_progresso = (ProgressBar)findViewById(R.id.progressbar_progresso);
        _progressbar_progresso.setVisibility(View.GONE);
        _button_salvar_resposta.setVisibility(View.GONE);
        provaId = Persistence.getPreferences(this).getInt(getString(R.string.id_prova),0);
        qtdQuestoes = getQuantidadeQuestoes();
        questoesCertas = Persistence.getDb(this).questaoDAO().getQuestaoDaProva(provaId);


    }
    public int getQuantidadeQuestoes(){
        Prova prova = Persistence.getDb(this).provaDAO().getProva(provaId);
        String modeloDeGabarito = prova.getModeloDeGabarito();
        if(modeloDeGabarito.equalsIgnoreCase(getString(R.string.questoes_10))){
            return 10;
        }else if(modeloDeGabarito.equalsIgnoreCase(getString(R.string.questoes_20))){
            return 20;
        }else if(modeloDeGabarito.equalsIgnoreCase(getString(R.string.questoes_50))){
            return 50;
        }else{
            return 100;
        }
    }

    Mat quad;

    public void corrigirProva() {

        listaRa = Persistence.getDb(this).respostaDAO().getListaRA(provaId);
        for(String r : listaRa) {
            if(r.toLowerCase().contains(ra.toLowerCase())) {
                respostaSalvas = Persistence.getDb(this).respostaDAO().getRespostasRa(ra);
            }
        }

        _progressbar_progresso.setVisibility(View.VISIBLE);
        Thread th = new Thread(){
            @Override
            public void run(){
                try {


                    Mat img = Imgcodecs.imread(mediaStorageDir().getPath() + "/" + "teste2.jpg");
                    if (img.empty()) {
                        Log.d("FragmentMain", "Empty Image");
                    }
                    Mat gray = new Mat();
                    Mat thresh = new Mat();
                    Imgproc.cvtColor(img, gray, Imgproc.COLOR_BGR2GRAY);
                    Imgproc.threshold(gray, thresh, 0, 255, Imgproc.THRESH_BINARY_INV + Imgproc.THRESH_OTSU);
                    Mat temp = thresh.clone();
                    Mat hierarchy = new Mat();
                    Mat corners = new Mat(4, 1, CvType.CV_32FC2);
                    List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
                    Imgproc.findContours(temp, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
                    hierarchy.release();
                    for (int idx = 0; idx < contours.size(); idx++) {
                        MatOfPoint contour = contours.get(idx);
                        MatOfPoint2f contour_points = new MatOfPoint2f(contour.toArray());
                        RotatedRect minRect = Imgproc.minAreaRect(contour_points);
                        Point[] rect_points = new Point[4];
                        minRect.points(rect_points);
                        if (minRect.size.height > img.width() / 2) {
                            List<Point> srcPoints = new ArrayList<Point>(4);
                            srcPoints.add(rect_points[2]);
                            srcPoints.add(rect_points[3]);
                            srcPoints.add(rect_points[0]);
                            srcPoints.add(rect_points[1]);
                            corners = Converters.vector_Point_to_Mat(srcPoints, CvType.CV_32F);
                        }
                    }

                    Imgproc.erode(thresh, thresh, new Mat(), new Point(-1, -1), 10);
                    Imgproc.dilate(thresh, thresh, new Mat(), new Point(-1, -1), 5);

                    //storeImage(thresh);
                    Mat results = new Mat(1000, 1000, CvType.CV_8UC3);
                    quad = new Mat(1000, 1000, CvType.CV_8UC1);
                    List<Point> dstPoints = new ArrayList<Point>(4);
                    dstPoints.add(new Point(0, 0));
                    dstPoints.add(new Point(1000, 0));
                    dstPoints.add(new Point(1000, 1000));
                    dstPoints.add(new Point(0, 1000));
                    Mat quad_pts = Converters.vector_Point_to_Mat(dstPoints, CvType.CV_32F);
                    Mat transmtx = Imgproc.getPerspectiveTransform(corners, quad_pts);
                    Imgproc.warpPerspective(img, results, transmtx, new Size(1000, 1000));
                    Imgproc.warpPerspective(thresh, quad, transmtx, new Size(1000, 1000));
                    Imgproc.resize(quad, quad, new Size(10, (qtdQuestoes / 2)));
                    Imgcodecs.imwrite("results.png", quad);
                    storeImage(quad);

                    int q = 0;
                    for (int c = 0; c < quad.cols(); c++) {
                        double[] d = new double[0];
                        d = quad.get(0, c);
                        if (d[0] != 0) {
                            q++;
                        }

                    }
                    if (q > 0) {
                        Core.rotate(quad, quad, Core.ROTATE_90_CLOCKWISE);

                    }

                    //showImage(results);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            corrigir(quad);
                        }
                    });
                }catch (Exception e){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(),"Ocorreu um erro, tente novamente",Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }


        };
        th.start();

    }

    public void corrigir(Mat mat) {


        StringBuilder opcoesAluno = new StringBuilder();
        StringBuilder opcoesCertas= new StringBuilder();
        int linhaInicial = 1;
        int m = 1;
        int q = qtdQuestoes/10;
        int qtdCertas =0;





        for (int i = 0; i < q; i++) {
            for (int c = 0; c < mat.cols(); c++) {



                int index = (c+m)-1;

                Questao questao = questoesCertas.get(index);
                Resposta resposta;
                if(respostaSalvas.size() > 0){
                    resposta = respostaSalvas.get(index);
                    for(Resposta r:respostaSalvas){

                    }
                }else{
                    resposta = new Resposta();
                    int t = (int)(-1*System.currentTimeMillis());
                    int id = t/(questao.getNumero());
                    resposta.setId(id);
                }


                int qm = 0;
                String opcaoMarcada = "";
                double d[] = new double[0];
                d = mat.get(linhaInicial, c);
                if (d[0] != 0) {
                    qm++;
                    opcaoMarcada = "A";
                }
                d = mat.get((linhaInicial + 1), c);
                if (d[0] != 0) {
                    qm++;
                    opcaoMarcada = "B";
                }
                d = mat.get((linhaInicial + 2), c);
                if (d[0] != 0) {
                    qm++;
                    opcaoMarcada = "C";
                }
                d = mat.get((linhaInicial + 3), c);
                if (d[0] != 0) {
                    qm++;
                    opcaoMarcada = "D";
                }

                if (qm >= 2 || qm == 0) {
                    opcaoMarcada = "*";
                }
                String numeroQuestao = formatarTextoQuestao(questao.getNumero());

                if(opcaoMarcada.equalsIgnoreCase(questao.getOpcaoCorreta())){
                    qtdCertas++;
                }

                opcoesAluno.append(numeroQuestao + " - " + opcaoMarcada + "\n");
                opcoesCertas.append(numeroQuestao+" - "+questao.getOpcaoCorreta()+"\n");


                resposta.setRa(ra);
                resposta.setQuestaoId(questao.getId());

                resposta.setOpcaoMarcada(opcaoMarcada);
                respostasAluno.add(resposta);

            }
            linhaInicial += 5;
            m += 10;

        }

        _textview_gabaritoAluno.setText(opcoesAluno.toString());
        _textview_gabaritoCerto.setText(opcoesCertas.toString());
        _textview_acertos.setText(qtdCertas+" / "+questoesCertas.size());
        _progressbar_progresso.setVisibility(View.GONE);
        _button_salvar_resposta.setVisibility(View.VISIBLE);

    }

    public String formatarTextoQuestao(int q){

        String questao = q < 10?"0"+q:q+"";
        return questao;
    }

    public void salvarResposta(View view){
        if(!isSalva){
            if(respostaSalvas.size() > 0){
                Persistence.getDb(this).respostaDAO().atualizarResposta(respostasAluno);
                Toast.makeText(this,"Atualizado com sucesso",Toast.LENGTH_SHORT).show();
            }else{
                Persistence.getDb(this).respostaDAO().inserirResposta(respostasAluno);
                Toast.makeText(this,"Salvo com sucesso",Toast.LENGTH_SHORT).show();



            }

            isSalva = true;
        }else{
            Toast.makeText(this,"Respostas já estão salvas",Toast.LENGTH_SHORT).show();
        }
    }


    public void getRA(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.desing_ra,null);
        Button _btnOk =  view.findViewById(R.id.button_ok);
        final EditText _edt_ra = view.findViewById(R.id.editText_ra);
        builder.setView(view);
        final AlertDialog alertDialog =  builder.create();
        alertDialog.show();
        _btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ra = _edt_ra.getText().toString();
                if(!ra.equals("")){
                    corrigirProva();
                    alertDialog.dismiss();

                }else{
                    Toast.makeText(getApplicationContext(),"Insira o RA",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }




    public File mediaStorageDir () {
        File _mediaStorageDir = new File(Environment.getExternalStorageDirectory()+"/Android/data");
        return _mediaStorageDir;
    }

    public void storeImage(Mat matImg) {
        Bitmap bitmapImg = Bitmap.createBitmap(matImg.cols(), matImg.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(matImg, bitmapImg);
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="IMG.jpg";
        mediaFile = new File(mediaStorageDir().getPath() + File.separator + mImageName);
        File pictureFile = mediaFile;
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmapImg.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("FragmentMain", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("FragmentMain", "Error accessing file: " + e.getMessage());
        }
    }

    @Override
    public void onClick(View view) {
        if(ra.equals("")  ){
            getRA();
        }else{
            if(respostasAluno.size() == 0){
                corrigirProva();
            }else{
                Toast.makeText(getApplicationContext(),"Prova já corrigida",Toast.LENGTH_SHORT).show();
            }

        }
    }
}
