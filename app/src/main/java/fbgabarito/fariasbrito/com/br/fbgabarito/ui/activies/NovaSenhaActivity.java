package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.Persistence;
import fbgabarito.fariasbrito.com.br.fbgabarito.persistence.entities.Usuario;

public class NovaSenhaActivity extends AppCompatActivity {

    private EditText _edtSenha;
    private EditText _edtConfirmar;
    private Button _btnEnviar;
    private String senha;
    private String c_senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_senha);

        _edtSenha = (EditText) findViewById(R.id.txtSenha);
        _edtConfirmar = (EditText) findViewById(R.id.txtConfirma);
        _btnEnviar = (Button) findViewById(R.id.btnEnviar);

        _btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validaCampos(v);
                startActivity(new Intent(NovaSenhaActivity.this, LoginActivity.class));
            }
        });

    }

    private void validaCampos(View view) {
        boolean res = false;

        senha = _edtSenha.getText().toString();
        c_senha = _edtConfirmar.getText().toString();

        if (res = isCampoVazio(senha)) {
            _edtSenha.requestFocus();
        } else if (res = isCampoVazio(c_senha)) {
            _edtConfirmar.requestFocus();
        } else {
                comparaCampos(senha, c_senha, view);
        }
    }

        private boolean comparaCampos(String valor1, String valor2, View view){

        boolean resultado = false;

        if(resultado = senha.equals(c_senha)){
            Usuario user = Persistence.getDb(this).usuarioDAO().getUsuario(this.getIntent().getExtras().getString("email"));
            user.setSenha(senha);
            Persistence.getDb(this).usuarioDAO().atualizarUsuario(user);
            Snackbar.make(view, "Senha atualizada com sucesso", Snackbar.LENGTH_LONG).show();

        }else{
            Snackbar.make(view, "Senhas não correspondem", Snackbar.LENGTH_SHORT).show();
        }

        return resultado;
        }


        private boolean isCampoVazio (String valor){
            boolean res = (TextUtils.isEmpty(valor) || valor.trim().isEmpty());
            return res;
        }
    }

