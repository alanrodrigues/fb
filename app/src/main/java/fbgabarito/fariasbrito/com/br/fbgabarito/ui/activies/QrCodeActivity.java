package fbgabarito.fariasbrito.com.br.fbgabarito.ui.activies;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.google.zxing.qrcode.encoder.QRCode;

import fbgabarito.fariasbrito.com.br.fbgabarito.R;

public class QrCodeActivity extends AppCompatActivity {

    String ra;
    Button _btn_Scannear;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
        _btn_Scannear = findViewById(R.id.btnScan);

        IntentIntegrator integrator = new IntentIntegrator(QrCodeActivity.this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setCameraId(0);
        integrator.setPrompt("Scanneando");
        integrator.setBeepEnabled(true);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();


    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if(result.getContents() != null){
                getRA(result.getContents());
            }else{
                getRA("Erro");
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    public void getRA(String codigo){
        ra = codigo;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Confirme o RA");
        View view = getLayoutInflater().inflate(R.layout.desing_ra,null);
        Button _btnOk =  view.findViewById(R.id.button_ok);
        final EditText _edt_ra = view.findViewById(R.id.editText_ra);
        _edt_ra.setText(ra);
        builder.setView(view);
        final AlertDialog alertDialog =  builder.create();
        alertDialog.show();
        _btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ra = _edt_ra.getText().toString();
                if(!ra.equals("")){
                    Intent dados = new Intent(getApplicationContext(), CorrigirProvaActivity.class);
                    dados.putExtra("ra", ra);
                    startActivity(dados);
                    alertDialog.dismiss();
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Insira o RA",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
